


from django.db import models
from django.conf import settings 

USER_MODEL = settings.AUTH_USER_MODEL

class ShoppingItem(models.Model):

    user = models.ForeignKey ( 
        USER_MODEL,
        on_delete = models.CASCADE,)


    food_item = models.ForeignKey (
        "FoodItem",
        on_delete=models.PROTECT,)

