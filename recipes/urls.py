from django.urls import path


from recipes.views import (
    delete_all_shopping_items,
    ShoppingItemListView,
    create_shopping_item, 
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
)



urlpatterns = [
    
    #Register that function view in the recipes Django app's urls.py

    path ("shopping_items/delete", delete_all_shopping_items, name = "delete_all_shopping_items"), 

    # Set the "delete_all_shopping_items" function view to handle the path "recipes/shopping_items/delete/"
    # and give it the name "delete_all_shopping_items" to use to refer to in Django url tags, the redirect
    # function, or the reverse/reverse_lazy functions



    #Register that class view in the recipes Django app's urls.py
    
    path ("shopping_items/", ShoppingItemListView.as_view(), name ="shopping_item_list"), 
    
    # Set the "ShoppingItemListView" class view to handle the path "recipes/shopping_items/"
    # and give it the name "shopping_item_list" to use to refer to in Django url tags, the redirect
    # function, or the reverse/reverse_lazy functions


    
    #First feature: add a food item to a shopping list
    path ("shopping_items/create/", create_shopping_item, name = "shopping_item_create"),
    #Register that function view in the recipes Django app's urls.py
    
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
