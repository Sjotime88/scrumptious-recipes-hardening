
from django.urls import path 

from meal_plans.views import (

    MealPlansListView, MealPlansCreateView, MealPlansDetailView, MealPlansEditView, MealPlansDeleteView, 
)
   
from recipes.views import (

    RecipeDeleteView, 
)

urlpatterns = [
    path ("", MealPlansListView.as_view(), name = "mealplans_list"),
    path ("create", MealPlansCreateView.as_view(), name = "mealplan_create"), 
    path ("<int:pk>/", MealPlansDetailView.as_view(),name = "mealplan_detail"),
    path("<int:pk>/", MealPlansEditView.as_view(), name = "mealplans_edit/"), 
    path ("<int:pk>/edit/", MealPlansDeleteView.as_view(), name = "mealplans_delete/"), 
    path ("<int:pk>/delete/", RecipeDeleteView.as_view(), name = "recipe_delete/"), 
]
