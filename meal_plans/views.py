from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import MealPlan 
     

# Create your views here.

class MealPlansListView(ListView,LoginRequiredMixin):
    model = MealPlan 
    template_name= "meal_plans/list.html"
# create link on page to the create view so people can create a new meal plan 
    paginate_by = 2 

#the name of each meal plan should be a link to its detail view. This is similar to the list view for recipes, wherethe name of each recipe is a link to the recipe's detial page. 

class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan 
    template = "meal_plans/create.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans.detail")

    def form_valid(self,form):
        form.instance.mealplan_recipes = self.request.user
        return super().form_valid(form)

    def form_valid(self,form):
        #save the meal plan but dont put it in the DB
        plan = form.save(commit=False)

        #assign the owner to the meal plans
        plan.owner = self.request.user

        #now assingn it to the DB
        plan.save

        #save all of the many-to-many relatoinships
        form.save_m2m()

        #redirect to the detail page for the meal plan 
        return redirect("meal_plan_detail", pk=plan.id)


#the name of each meal plan snould link to its detail view. Similar to the list view for recipes, where the name of each recipe is a link to the recipes detail page. 

class MealPlansDetailView(DetailView, LoginRequiredMixin):
    model = MealPlan 
    template = "meal_plans/detail.html"

#In the meal plans project, we wanted to filter the results that people would see so that they would only see the ones they created. For any of those views, that meant filtering out the data that the user did not "own".

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)  

   
     
class MealPlansEditView (UpdateView, LoginRequiredMixin): 
    model = MealPlan 
    template_name = "mealplans/edit.html"
    fields = ["owner"]

class MealPlansDeleteView (DeleteView): 
    model = MealPlan 
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplans_list")


